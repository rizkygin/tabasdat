<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTravelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tujuan_perjalanan');
            $table->string('transportasi');
            $table->string('tempat_berangkat');
            $table->string('tempat_tujuan');
            $table->integer('waktu_id')->unsigned();
            $table->timestamps();

            $table->foreign('waktu_id')
                ->references('id')
                ->on('times');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travels');
    }
}
