<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('letters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nomor_surat');
            $table->integer('perjalanan_id')->unsigned();
            $table->integer('pegawai_pembuat_id')->unsigned();
            $table->integer('pegawai_id')->unsigned();
            $table->timestamps();

            $table->foreign('perjalanan_id')
            ->references('id')
            ->on('travels');

            $table->foreign('pegawai_pembuat_id')
            ->references('id')
            ->on('users');

            $table->foreign('pegawai_id')
            ->references('id')
            ->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('letters');
    }
}
