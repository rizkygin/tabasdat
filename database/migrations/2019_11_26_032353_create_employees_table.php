<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_pegawai');
            $table->string('nip_pegawai');
            $table->integer('jabatan_id')->unsigned();
            $table->integer('pangkat_id')->unsigned();
            $table->timestamps();

            $table->foreign('jabatan_id')
            ->references('id')
            ->on('positions');

            $table->foreign('pangkat_id')
            ->references('id')
            ->on('grades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
