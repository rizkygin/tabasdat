<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_pangkat');
            $table->integer('golongan_id')->unsigned();
            $table->integer('ruangan_id')->unsigned();
            $table->timestamps();

            $table->foreign('golongan_id')
            ->references('id')
            ->on('groups');

            $table->foreign('ruangan_id')
            ->references('id')
            ->on('rooms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grades');
    }
}
