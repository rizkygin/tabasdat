<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class Costseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {    
        $faker = Faker::create('id_ID');

            $waktu = $faker->dateTimeThisYear($max = 'now', $timezone = null);
            $items = [
                [
                    'total_biaya' => $faker->numberBetween($min = 1000000, $max = 10000000),
                    'mata_anggaran' => 'APBN',
                    'created_at' => $waktu,
                    'updated_at' => $waktu
                ],[
                    'total_biaya' => $faker->numberBetween($min = 1000000, $max = 10000000),
                    'mata_anggaran' => 'APBN',
                    'created_at' => $waktu,
                    'updated_at' => $waktu
                ],[
                    'total_biaya' => $faker->numberBetween($min = 1000000, $max = 10000000),
                    'mata_anggaran' => 'APBN',
                    'created_at' => $waktu,
                    'updated_at' => $waktu
                ],[
                    'total_biaya' => $faker->numberBetween($min = 1000000, $max = 10000000),
                    'mata_anggaran' => 'APBN',
                    'created_at' => $waktu,
                    'updated_at' => $waktu
                ],[
                    'total_biaya' => $faker->numberBetween($min = 1000000, $max = 10000000),
                    'mata_anggaran' => 'APBN',
                    'created_at' => $waktu,
                    'updated_at' => $waktu
                ],[
                    'total_biaya' => $faker->numberBetween($min = 1000000, $max = 10000000),
                    'mata_anggaran' => 'APBN',
                    'created_at' => $waktu,
                    'updated_at' => $waktu
                ],[
                    'total_biaya' => $faker->numberBetween($min = 1000000, $max = 10000000),
                    'mata_anggaran' => 'APBN',
                    'created_at' => $waktu,
                    'updated_at' => $waktu
                ],[
                    'total_biaya' => $faker->numberBetween($min = 1000000, $max = 10000000),
                    'mata_anggaran' => 'APBN',
                    'created_at' => $waktu,
                    'updated_at' => $waktu
                ],[
                    'total_biaya' => $faker->numberBetween($min = 1000000, $max = 10000000),
                    'mata_anggaran' => 'APBN',
                    'created_at' => $waktu,
                    'updated_at' => $waktu
                ],[
                    'total_biaya' => $faker->numberBetween($min = 1000000, $max = 10000000),
                    'mata_anggaran' => 'APBN',
                    'created_at' => $waktu,
                    'updated_at' => $waktu
                ],[
                    'total_biaya' => $faker->numberBetween($min = 1000000, $max = 10000000),
                    'mata_anggaran' => 'APBN',
                    'created_at' => $waktu,
                    'updated_at' => $waktu
                ],[
                    'total_biaya' => $faker->numberBetween($min = 1000000, $max = 10000000),
                    'mata_anggaran' => 'APBN',
                    'created_at' => $waktu,
                    'updated_at' => $waktu
                ],[
                    'total_biaya' => $faker->numberBetween($min = 1000000, $max = 10000000),
                    'mata_anggaran' => 'APBN',
                    'created_at' => $waktu,
                    'updated_at' => $waktu
                ],[
                    'total_biaya' => $faker->numberBetween($min = 1000000, $max = 10000000),
                    'mata_anggaran' => 'APBN',
                    'created_at' => $waktu,
                    'updated_at' => $waktu
                ],[
                    'total_biaya' => $faker->numberBetween($min = 1000000, $max = 10000000),
                    'mata_anggaran' => 'APBN',
                    'created_at' => $waktu,
                    'updated_at' => $waktu
                ],[
                    'total_biaya' => $faker->numberBetween($min = 1000000, $max = 10000000),
                    'mata_anggaran' => 'APBN',
                    'created_at' => $waktu,
                    'updated_at' => $waktu
                ],[
                    'total_biaya' => $faker->numberBetween($min = 1000000, $max = 10000000),
                    'mata_anggaran' => 'APBN',
                    'created_at' => $waktu,
                    'updated_at' => $waktu
                ],[
                    'total_biaya' => $faker->numberBetween($min = 1000000, $max = 10000000),
                    'mata_anggaran' => 'APBN',
                    'created_at' => $waktu,
                    'updated_at' => $waktu
                ],[
                    'total_biaya' => $faker->numberBetween($min = 1000000, $max = 10000000),
                    'mata_anggaran' => 'APBN',
                    'created_at' => $waktu,
                    'updated_at' => $waktu
                ],[
                    'total_biaya' => $faker->numberBetween($min = 1000000, $max = 10000000),
                    'mata_anggaran' => 'APBN',
                    'created_at' => $waktu,
                    'updated_at' => $waktu
                ]
            ];
            foreach($items as $item){
                App\Cost::create($item);

            }
        
        
    }
}
