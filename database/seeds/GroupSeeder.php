<?php

use Illuminate\Database\Seeder;

class GroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        $seeders = [
            [
                'nama_golongan' => 'I'
            ],[
                'nama_golongan' => 'II'
               
            ],[
                'nama_golongan' => 'III'
               
            ],[
                'nama_golongan' => 'IV'
            ]
        ];
        foreach($seeders as $seeder){
            App\Group::create($seeder);
        }
    }
}
