<?php

use Illuminate\Database\Seeder;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        $seeders = [
            [
                'nama_ruang' => 'A'
            ],[
                'nama_ruang' => 'B'
               
            ],[
                'nama_ruang' => 'C'
               
            ],[
                'nama_ruang' => 'D'

            ],[
                'nama_ruang' => 'E'
            ]
        ];
        foreach($seeders as $seeder){
            App\Room::create($seeder);
        }
    }
}
