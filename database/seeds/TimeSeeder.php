<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class TimeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        $waktu = $faker->dateTimeThisYear($max = 'now', $timezone = null);
        $items = [
            [
                'lama_perjalanan' => $faker->numberBetween($min = 1000000, $max = 10000000),
                'tanggal_berangkat' => $waktu,
                'tanggal_harus_berangkat' => $waktu,
                'created_at' => $waktu,
            ],[
                'lama_perjalanan' => $faker->numberBetween($min = 1000000, $max = 10000000),
                'tanggal_berangkat' => $waktu,
                'tanggal_harus_berangkat' => $waktu,
                'created_at' => $waktu,
            ],[
                'lama_perjalanan' => $faker->numberBetween($min = 1000000, $max = 10000000),
                'tanggal_berangkat' => $waktu,
                'tanggal_harus_berangkat' => $waktu,
                'created_at' => $waktu,
            ],[
                'lama_perjalanan' => $faker->numberBetween($min = 1000000, $max = 10000000),
                'tanggal_berangkat' => $waktu,
                'tanggal_harus_berangkat' => $waktu,
                'created_at' => $waktu,
            ],[
                'lama_perjalanan' => $faker->numberBetween($min = 1000000, $max = 10000000),
                'tanggal_berangkat' => $waktu,
                'tanggal_harus_berangkat' => $waktu,
                'created_at' => $waktu,
            ],[
                'lama_perjalanan' => $faker->numberBetween($min = 1000000, $max = 10000000),
                'tanggal_berangkat' => $waktu,
                'tanggal_harus_berangkat' => $waktu,
                'created_at' => $waktu,
            ],[
                'lama_perjalanan' => $faker->numberBetween($min = 1000000, $max = 10000000),
                'tanggal_berangkat' => $waktu,
                'tanggal_harus_berangkat' => $waktu,
                'created_at' => $waktu,
            ],[
                'lama_perjalanan' => $faker->numberBetween($min = 1000000, $max = 10000000),
                'tanggal_berangkat' => $waktu,
                'tanggal_harus_berangkat' => $waktu,
                'created_at' => $waktu,
            ],[
                'lama_perjalanan' => $faker->numberBetween($min = 1000000, $max = 10000000),
                'tanggal_berangkat' => $waktu,
                'tanggal_harus_berangkat' => $waktu,
                'created_at' => $waktu,
            ],[
                'lama_perjalanan' => $faker->numberBetween($min = 1000000, $max = 10000000),
                'tanggal_berangkat' => $waktu,
                'tanggal_harus_berangkat' => $waktu,
                'created_at' => $waktu,
            ],[
                'lama_perjalanan' => $faker->numberBetween($min = 1000000, $max = 10000000),
                'tanggal_berangkat' => $waktu,
                'tanggal_harus_berangkat' => $waktu,
                'created_at' => $waktu,
            ],[
                'lama_perjalanan' => $faker->numberBetween($min = 1000000, $max = 10000000),
                'tanggal_berangkat' => $waktu,
                'tanggal_harus_berangkat' => $waktu,
                'created_at' => $waktu,
            ],[
                'lama_perjalanan' => $faker->numberBetween($min = 1000000, $max = 10000000),
                'tanggal_berangkat' => $waktu,
                'tanggal_harus_berangkat' => $waktu,
                'created_at' => $waktu,
            ],[
                'lama_perjalanan' => $faker->numberBetween($min = 1000000, $max = 10000000),
                'tanggal_berangkat' => $waktu,
                'tanggal_harus_berangkat' => $waktu,
                'created_at' => $waktu,
            ],[
                'lama_perjalanan' => $faker->numberBetween($min = 1000000, $max = 10000000),
                'tanggal_berangkat' => $waktu,
                'tanggal_harus_berangkat' => $waktu,
                'created_at' => $waktu,
            ],[
                'lama_perjalanan' => $faker->numberBetween($min = 1000000, $max = 10000000),
                'tanggal_berangkat' => $waktu,
                'tanggal_harus_berangkat' => $waktu,
                'created_at' => $waktu,
            ],[
                'lama_perjalanan' => $faker->numberBetween($min = 1000000, $max = 10000000),
                'tanggal_berangkat' => $waktu,
                'tanggal_harus_berangkat' => $waktu,
                'created_at' => $waktu,
            ],[
                'lama_perjalanan' => $faker->numberBetween($min = 1000000, $max = 10000000),
                'tanggal_berangkat' => $waktu,
                'tanggal_harus_berangkat' => $waktu,
                'created_at' => $waktu,
            ],[
                'lama_perjalanan' => $faker->numberBetween($min = 1000000, $max = 10000000),
                'tanggal_berangkat' => $waktu,
                'tanggal_harus_berangkat' => $waktu,
                'created_at' => $waktu,
            ],[
                'lama_perjalanan' => $faker->numberBetween($min = 1000000, $max = 10000000),
                'tanggal_berangkat' => $waktu,
                'tanggal_harus_berangkat' => $waktu,
                'created_at' => $waktu,
            ]
        ];            
        foreach($items as $item) { 
            App\Time::create($item);
        }

    }
}
