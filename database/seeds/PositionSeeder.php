<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $seeders =[
            [
                'instansi_id' => '2',
                'nama_jabatan' => $faker->jobTitle
            ],[
                'instansi_id' => '3',
                'nama_jabatan' => $faker->jobTitle
            ],[
                'instansi_id' => '4',
                'nama_jabatan' => $faker->jobTitle
            ],[
                'instansi_id' => '5',
                'nama_jabatan' => $faker->jobTitle
            ],[
                'instansi_id' => '6',
                'nama_jabatan' => $faker->jobTitle
            ],[
                'instansi_id' => '7',
                'nama_jabatan' => $faker->jobTitle
            ],[
                'instansi_id' => '8',
                'nama_jabatan' => $faker->jobTitle
            ],[
                'instansi_id' => '9',
                'nama_jabatan' => $faker->jobTitle
            ],[
                'instansi_id' => '10',
                'nama_jabatan' => $faker->jobTitle
            ],[
                'instansi_id' => '11',
                'nama_jabatan' => $faker->jobTitle
            ],[
                'instansi_id' => '12',
                'nama_jabatan' => $faker->jobTitle
            ],[
                'instansi_id' => '13',
                'nama_jabatan' => $faker->jobTitle
            ],[
                'instansi_id' => '14',
                'nama_jabatan' => $faker->jobTitle
            ],[
                'instansi_id' => '15',
                'nama_jabatan' => $faker->jobTitle
            ],[
                'instansi_id' => '16',
                'nama_jabatan' => $faker->jobTitle
            ],[
                'instansi_id' => '17',
                'nama_jabatan' => $faker->jobTitle
            ],[
                'instansi_id' => '18',
                'nama_jabatan' => $faker->jobTitle
            ],[
                'instansi_id' => '19',
                'nama_jabatan' => $faker->jobTitle
            ]
        ];
        foreach($seeders as $seeder){
            App\Position::create($seeder);
        }
    }
}
