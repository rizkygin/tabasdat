<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        $items = [
        [
            'name' => $faker->name,
            'email' => $faker->email,
            'nip' => 13445,
            'jabatan_id' => 3,
            'pangkat_id' => 3,
            'email_verified_at' => $faker->date,
            'password' => $faker->password,
            'remember_token'=> $faker->randomNumber,
            'created_at' => $faker->date
        ]
        ];
        foreach($items as $item) { 
            App\User::create($item);
        }
    }
}
