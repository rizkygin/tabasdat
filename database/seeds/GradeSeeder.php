<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class GradeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        $items =[
            [
                'nama_pangkat' => 'Juru Muda',
                'golongan_id' => 1,
                'ruangan_id' => 1,
                'created_at' => $faker->date
            ],
            [
                'nama_pangkat' => 'Juru Muda Tingkat 1',
                'golongan_id' => 1,
                'ruangan_id' => 2,
                'created_at' => $faker->date
            ],
            [
                'nama_pangkat' => 'Juru',
                'golongan_id' => 1,
                'ruangan_id' => 3,
                'created_at' => $faker->date
            ],
            [
                'nama_pangkat' => 'Juru Tingkat 1',
                'golongan_id' => 1,
                'ruangan_id' => 4,
                'created_at' => $faker->date
            ],
            [
                'nama_pangkat' => 'Pengatur Muda',
                'golongan_id' => 2,
                'ruangan_id' => 1,
                'created_at' => $faker->date
            ],
            [
                'nama_pangkat' => 'Pengatur Muda Tingkat 1',
                'golongan_id' => 2,
                'ruangan_id' => 2,
                'created_at' => $faker->date
            ],
            [
                'nama_pangkat' => 'Pengatur',
                'golongan_id' => 2,
                'ruangan_id' => 3,
                'created_at' => $faker->date
            ],
            [
                'nama_pangkat' => 'Pengatur Tingkat 1',
                'golongan_id' => 2,
                'ruangan_id' => 4,
                'created_at' => $faker->date
            ],
            [
                'nama_pangkat' => 'Penata Muda',
                'golongan_id' => 3,
                'ruangan_id' => 1,
                'created_at' => $faker->date
            ],
            [
                'nama_pangkat' => 'Penata Muda Tingkat 1',
                'golongan_id' => 3,
                'ruangan_id' => 2,
                'created_at' => $faker->date
            ],
            [
                'nama_pangkat' => 'Penata',
                'golongan_id' => 3,
                'ruangan_id' => 3,
                'created_at' => $faker->date
            ],
            [
                'nama_pangkat' => 'Penata Tingkat 1',
                'golongan_id' => 3,
                'ruangan_id' => 4,
                'created_at' => $faker->date
            ],
            [
                'nama_pangkat' => 'Pembina',
                'golongan_id' => 4,
                'ruangan_id' => 1,
                'created_at' => $faker->date
            ],
            [
                'nama_pangkat' => 'Pembina Tingkat 1',
                'golongan_id' => 4,
                'ruangan_id' => 2,
                'created_at' => $faker->date
            ],
            [
                'nama_pangkat' => 'Pembina Utama Muda',
                'golongan_id' => 4,
                'ruangan_id' => 3,
                'created_at' => $faker->date
            ],
            [
                'nama_pangkat' => 'Pembina Utama Madya',
                'golongan_id' => 4,
                'ruangan_id' => 4,
                'created_at' => $faker->date
            ],
            [
                'nama_pangkat' => 'Penata Utama',
                'golongan_id' => 4,
                'ruangan_id' => 5,
                'created_at' => $faker->date
            ],
            
        ];
        foreach($items as $item) { 
            App\Grade::create($item);
        }
    }
}
