<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(TimeSeeder::class);
        // $this->call(Costseeder::class);
        $this->call(DatabaseSeeder::class);
    }
}
