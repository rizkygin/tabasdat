<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        $items =[
            [
                'nama_pegawai' => $faker->name,
                'nip_pegawai' => $faker->randomNumber,
                'jabatan_id'=>3,
                'pangkat_id'=>3,
                'created_at'=>$faker->date
            ],
            [
                'nama_pegawai' => $faker->name,
                'nip_pegawai' => $faker->randomNumber,
                'jabatan_id'=>4,
                'pangkat_id'=>4,
                'created_at'=>$faker->date
            ],
            [
                'nama_pegawai' => $faker->name,
                'nip_pegawai' => $faker->randomNumber,
                'jabatan_id'=>5,
                'pangkat_id'=>5,
                'created_at'=>$faker->date
            ],
            [
                'nama_pegawai' => $faker->name,
                'nip_pegawai' => $faker->randomNumber,
                'jabatan_id'=>6,
                'pangkat_id'=>6,
                'created_at'=>$faker->date
            ],
            [
                'nama_pegawai' => $faker->name,
                'nip_pegawai' => $faker->randomNumber,
                'jabatan_id'=>7,
                'pangkat_id'=>7,
                'created_at'=>$faker->date
            ],
            [
                'nama_pegawai' => $faker->name,
                'nip_pegawai' => $faker->randomNumber,
                'jabatan_id'=>8,
                'pangkat_id'=>8,
                'created_at'=>$faker->date
            ],
            [
                'nama_pegawai' => $faker->name,
                'nip_pegawai' => $faker->randomNumber,
                'jabatan_id'=>9,
                'pangkat_id'=>9,
                'created_at'=>$faker->date
            ],
            [
                'nama_pegawai' => $faker->name,
                'nip_pegawai' => $faker->randomNumber,
                'jabatan_id'=>10,
                'pangkat_id'=>10,
                'created_at'=>$faker->date
            ],
            [
                'nama_pegawai' => $faker->name,
                'nip_pegawai' => $faker->randomNumber,
                'jabatan_id'=>11,
                'pangkat_id'=>11,
                'created_at'=>$faker->date
            ],
            [
                'nama_pegawai' => $faker->name,
                'nip_pegawai' => $faker->randomNumber,
                'jabatan_id'=>12,
                'pangkat_id'=>12,
                'created_at'=>$faker->date
            ],
            [
                'nama_pegawai' => $faker->name,
                'nip_pegawai' => $faker->randomNumber,
                'jabatan_id'=>13,
                'pangkat_id'=>13,
                'created_at'=>$faker->date
            ],
            [
                'nama_pegawai' => $faker->name,
                'nip_pegawai' => $faker->randomNumber,
                'jabatan_id'=>14,
                'pangkat_id'=>14,
                'created_at'=>$faker->date
            ],
            [
                'nama_pegawai' => $faker->name,
                'nip_pegawai' => $faker->randomNumber,
                'jabatan_id'=>15,
                'pangkat_id'=>15,
                'created_at'=>$faker->date
            ],
            [
                'nama_pegawai' => $faker->name,
                'nip_pegawai' => $faker->randomNumber,
                'jabatan_id'=>16,
                'pangkat_id'=>16,
                'created_at'=>$faker->date
            ],
            [
                'nama_pegawai' => $faker->name,
                'nip_pegawai' => $faker->randomNumber,
                'jabatan_id'=>17,
                'pangkat_id'=>17,
                'created_at'=>$faker->date
            ],
            [
                'nama_pegawai' => $faker->name,
                'nip_pegawai' => $faker->randomNumber,
                'jabatan_id'=>18,
                'pangkat_id'=>1,
                'created_at'=>$faker->date
            ],
            [
                'nama_pegawai' => $faker->name,
                'nip_pegawai' => $faker->randomNumber,
                'jabatan_id'=>19,
                'pangkat_id'=>2,
                'created_at'=>$faker->date
            ]
        ];
        foreach($items as $item){
            App\Employee::create($item);
        }
    }
}
