<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;


class LetterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    $faker = Faker::create('id_ID');

        $waktu = $faker->dateTimeThisYear($max = 'now', $timezone = null);
        $items = [
            [
            'nomor_surat' => '461/07/X/2016',
            'perjalanan_id' => '3',
            'pegawai_pembuat_id' =>'3',
            'pegawai_id'=>'3',
            'created_at' => $waktu,

            ],[
            'nomor_surat' => '462/07/X/2016',
            'perjalanan_id' => '4',
            'pegawai_pembuat_id' =>'4',
            'pegawai_id'=>'4',
            'created_at' => $waktu,
        ],[
            'nomor_surat' => '463/07/X/2016',
            'perjalanan_id' => '5',
            'pegawai_pembuat_id' =>'5',
            'pegawai_id'=>'5',
            'created_at' => $waktu,
                
            ],[
            'nomor_surat' => '464/07/X/2016',
            'perjalanan_id' => '6',
            'pegawai_pembuat_id' =>'6',
            'pegawai_id'=>'6',
            'created_at' => $waktu,
        ],[
                'nomor_surat' => '465/07/X/2016',
                'perjalanan_id' => '7',
                'pegawai_pembuat_id' =>'7',
                'pegawai_id'=>'7',
                'created_at' => $waktu,
            ],[
                'nomor_surat' => '466/07/X/2016',
                'perjalanan_id' => '8',
                'pegawai_pembuat_id' =>'8',
                'pegawai_id'=>'8',
                'created_at' => $waktu,
            ],[
                'nomor_surat' => '467/07/X/2016',
                'perjalanan_id' => '9',
                'pegawai_pembuat_id' =>'9',
                'pegawai_id'=>'9',
                'created_at' => $waktu,
            ],[
                'nomor_surat' => '468/07/X/2016',
                'perjalanan_id' => '10',
                'pegawai_pembuat_id' =>'10',
                'pegawai_id'=>'10',
                'created_at' => $waktu,
            ],[
                'nomor_surat' => '469/07/X/2016',
                'perjalanan_id' => '11',
                'pegawai_pembuat_id' =>'11',
                'pegawai_id'=>'11',
                'created_at' => $waktu,
            ],[
                'nomor_surat' => '470/07/X/2016',
                'perjalanan_id' => '12',
                'pegawai_pembuat_id' =>'12',
                'pegawai_id'=>'12',
                'created_at' => $waktu,
            ],[
                'nomor_surat' => '471/07/X/2016',
                'perjalanan_id' => '13',
                'pegawai_pembuat_id' =>'13',
                'pegawai_id'=>'13',
                'created_at' => $waktu,
            ],[
                'nomor_surat' => '472/07/X/2016',
                'perjalanan_id' => '14',
                'pegawai_pembuat_id' =>'14',
                'pegawai_id'=>'14',
                'created_at' => $waktu,
            ],[
                'nomor_surat' => '473/07/X/2016',
                'perjalanan_id' => '15',
                'pegawai_pembuat_id' =>'15',
                'pegawai_id'=>'15',
                'created_at' => $waktu,
            ],[
                'nomor_surat' => '474/07/X/2016',
                'perjalanan_id' => '16',
                'pegawai_pembuat_id' =>'16',
                'pegawai_id'=>'16',
                'created_at' => $waktu,
            ],[
                'nomor_surat' => '475/07/X/2016',
                'perjalanan_id' => '17',
                'pegawai_pembuat_id' =>'17',
                'pegawai_id'=>'17',
                'created_at' => $waktu,
            ],[
                'nomor_surat' => '476/07/X/2016',
                'perjalanan_id' => '18',
                'pegawai_pembuat_id' =>'18',
                'pegawai_id'=>'18',
                'created_at' => $waktu,
            ]
        ];            
        foreach($items as $item) { 
            App\Letter::create($item);
        }

    }
}

