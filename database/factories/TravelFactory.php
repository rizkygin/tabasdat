<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Travel;
use Faker\Generator as Faker;

$factory->define(Travel::class, function (Faker $faker) {
    return [
        'tujuan_perjalanan' => $faker->state,
        'transportasi' => 'motor pribadi',
        'tempat_berangkat'  => 'BAPEDA BANYUMAS',
        'tempat_tujuan'  => $faker->city,
        'waktu_id'  => $faker->numberBetween($min = 1, $max = 20),
        'created_at' => $faker->dateTimeThisYear($max = 'now', $timezone = null)
    ];
});
