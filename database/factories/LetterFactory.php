<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Letter;
use Faker\Generator as Faker;

$factory->define(Letter::class, function (Faker $faker) {
    return [
        'no_surat' => $faker->numberBetween($min = 1, $max = 20),
        'perjalanan_id' => $faker->numberBetween($min = 2, $max = 21),
        'pegawai_pembuat_id' =>$faker->numberBetween($min = 2, $max = 21),
        'pegawai_id'=>$faker->numberBetween($min = 2, $max = 21),
        'created_at' => $faker->dateTimeThisYear($max = 'now', $timezone = null)
    ];
});
