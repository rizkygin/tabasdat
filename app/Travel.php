<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Travel extends Model
{
    protected $table = 'travels';
    public function cost(){
        return $this->belongsTo('App\Cost', 'biaya_id' );
    
    }

    public function letter()
    {
        return $this->hasOne('App\Letter','perjalanan_id');
    }

    public function times()
    {
        return $this->belongsTo('App\Time', 'waktu_id');
    }
}
