<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $table = 'grades';
    public function users()
    {
        return $this->hasOne('App\User', 'pegawai_id');
    }

    public function groups()
    {
        return $this->belongsTo('App\Group', 'golongan_id');
    }
    
    public function rooms()
    {
        return $this->belongsTo('App\Room', 'ruangan_id');
    }

    public function employees()
    {
        return $this->hasOne('App\Employee', 'pegawai_id');
    }


}
