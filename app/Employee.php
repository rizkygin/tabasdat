<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employees';
    public function letters()
    {
        return $this->belongsTo('App\Letter', 'pegawai_id');
    }

    public function positions()
    {
        return $this->belongsTo('App\Position', 'jabatan_id');
    }

    public function grades()
    {
        return $this->belongsTo('App\Grade', 'pangkat_id');
    }
}
