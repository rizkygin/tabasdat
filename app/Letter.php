<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Letter extends Model
{
    protected $table = 'letters';
    public function travels(){
        return $this->belongsTo('App\Travel', 'perjalanan_id');
    
    }

    public function users(){
        return $this->belongsTo('App\User', 'pegawai_pembuat_id');
    }

    public function employees()
    {
        return $this->hasMany('App\Employee', 'pegawai_id');
    }
}
