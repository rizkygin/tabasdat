<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';
    public function grades()
    {
        return $this->hasOne('App\Grade', 'pangkat_id');
    }
    
}
