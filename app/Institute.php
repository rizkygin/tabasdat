<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institute extends Model
{
    protected $table = 'institutes';
    public function positions()
    {
        return $this->hasOne('App\Position', 'jabatan_id');
    }
}
