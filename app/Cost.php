<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Cost extends Model
{
    
    protected $table = 'costs';
    public function travel()
    {
        return $this->hasOne('App\Travel', 'perjalanan_id');
    }
}
