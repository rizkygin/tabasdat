<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Time extends Model
{
    protected $table = 'times';
    public function travel(){
        return $this->hasOne('App\Travel', 'perjalanan_id');
    
    }
}
