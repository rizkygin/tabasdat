<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $table = 'positions';
    public function users()
    {
        return $this->hasOne('App\User', 'pegawai_id');
    }

    public function institutes(){
        return $this->belongsTo('App\Institut', 'instansi_id' );
    
    }
}
